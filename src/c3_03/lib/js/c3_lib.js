function c3FromData(data) {
  let columns = [];
  let types = {};
  let categories = []

  // categories - leo las categorías del primer registro
  for (let prop in data[0]) {
    if (prop !== 'name' && prop !== 'type') categories.push(prop);
  };

  data.forEach(function(record) {
    // types
    if (record.type) types[record.name] = record.type;

    let column = [record.name];
    categories.forEach(function(category) {
      column.push(record[category]);
    });
    columns.push(column);
  });

  const ret = {
    columns: columns,
    types: types,
    categories: categories
  };

  return ret;
};