function c3Chart(htmlId, data, categories, xLabel, yLabel) {

  const config = c3FromData(data, categories);

  let x = {
    type: 'category',
    categories: categories,
    show: true
  };

  if (xLabel) x.label = { text: xLabel, position: 'outer-middle' }

  let y = {};
  if (yLabel) y.label = { text: yLabel, position: 'outer-middle' }

  const chart = c3.generate({
    bindto: htmlId,
    data: {
      columns: config.columns,
      types: config.types
    },
    axis: {
      y: y,
      x: x
    }
  });

  function c3FromData(data, categories) {
    let columns = [];
    let types = {};

    data.sort(function(a, b) {
      return a.Name < b.Name ? -1 : ( a.Name > b.Name ? 1 : 0);
    });

    data.forEach(function(record) {
      // types
      if (record.type) types[record.Name] = record.type;

      let column = [record.Name];
      categories.forEach(function(category) {
        column.push(record[category]);
      });
      columns.push(column);
    });

    const ret = {
      columns: columns,
      types: types
    };

    return ret;
  };

};