function initMap(htmlId, center, locations, zoom) {

  // valores por defecto
  if (center === undefined) center = [51.507567, -0.112267];
  if (zoom === undefined) zoom = 13;
  if (locations === undefined) locations = [];

  let mymap = L.map(htmlId).setView(center, zoom);

  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
      '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
      'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox.streets'
  }).addTo(mymap);

  let popup = L.popup();

  function onMapClick(e) {
    console.log(e.latlng);
    popup
      .setLatLng(e.latlng)
      .setContent("You clicked " + e.latlng.toString())
      .openOn(mymap);
  }

  mymap.on('click', onMapClick);

  locations.forEach(function (location) {
    let marker = L.marker([location.lat, location.lon]).addTo(mymap);
    marker.bindPopup(location.nombre);
  });

  return mymap;
};