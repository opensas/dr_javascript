# Notas para el workshop


## Ejemplo 1: ¿Qué es JavaScript?

```
    <style>
      p {
        font-family: 'helvetica neue', helvetica, sans-serif;
        letter-spacing: 1px;
        text-transform: uppercase;
        text-align: center;
        border: 2px solid rgba(0,0,200,0.6);
        background: rgba(0,0,200,0.3);
        color: rgba(0,0,200,0.6);
        box-shadow: 1px 1px 2px rgba(0,0,200,0.4);
        border-radius: 10px;
        padding: 3px 10px;
        display: inline-block;
        cursor:pointer;
      }
    </style>
```

```
    <script>
      var parrafo = document.querySelector('p');

      parrafo.addEventListener('click', actualizarNombre);

      function actualizarNombre() {
        var nombre = prompt('¿Cómo te llamás?');
        if (nombre === '') nombre = 'desconocido';
        parrafo.innerHTML = 'Participante temeroso: <strong>' + nombre + '</strong>';
      }
    </script>
```