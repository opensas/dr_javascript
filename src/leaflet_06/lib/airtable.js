function airtableLoadSheet(sheetId, apiKey, callback) {

  const log = true;

  //https://api.airtable.com/v0/appSLzp1WuL9ik8fJ/Table%201?api_key=keyx7FrPMoI1SDzTj
  const url = 'https://api.airtable.com/v0/' + sheetId + '?api_key=' + apiKey;

  if (log) console.log('about to fetch', url);
  jsonFetch(url, function (json) {
    let records = getFields(json);
    if (log) console.log(JSON.stringify(records));
    callback(records);
  });

  function getFields(table) {
    let records = [];
    table.records.forEach(function(record) {
      records.push(record.fields);
    });
    return records;
  }

};
