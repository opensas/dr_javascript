function gdocsCargarPlanilla(planillaUrl, callback) {

  Tabletop.init({
    key: planillaUrl,
    callback: callback,
    simpleSheet: true
  })

};

function gdocsCargarPlanillaOffline(planillaUrl, callback) {

  var delay = 500;

  // offline hack
  var result = [
    {"Name":"fuente 1","type":"spline","Ene":"30","Feb":"200","Mar":"100","Abr":"400","May":"150","Jun":"250"},
    {"Name":"fuente 2","type":"area","Ene":"50","Feb":"20","Mar":"10","Abr":"40","May":"15","Jun":"25"},
    {"Name":"fuente 3","type":"area-step","Ene":"10","Feb":"150","Mar":"25","Abr":"370","May":"48","Jun":"430"},
    {"Name":"fuente 4","type":"bar","Ene":"5","Feb":"35","Mar":"15","Abr":"280","May":"260","Jun":"20"},
    {"Name":"fuente nueva","type":"area","Ene":"10","Feb":"20","Mar":"30","Abr":"40","May":"50","Jun":"60"}
  ];
  
  console.log('about to simluate TableTop call in ' + delay + ' ms.');

  setTimeout(function() {
    callback(result)
  }, delay);

}