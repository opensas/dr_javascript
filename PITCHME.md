---?image=assets/dr_js/img01.jpg

## @color[#cc0000](Dr. JavaScript)

- @color[#cc0000](O cómo aprendí a dejar de <br />preocuparme y AMAR JavaScript) |

---

### Algunos links útiles

- Repositorio: [gitlab.com/opensas/dr_javascript](https://gitlab.com/opensas/dr_javascript)
- Presentación: [gitpitch.com/opensas/dr_javascript?grs=gitlab](https://gitpitch.com/opensas/dr_javascript?grs=gitlab#/)
- Twitter: [@opensas](https://twitter.com/opensas)
- Mail: [opensas@gmail.com](opensas@gmail.com)

---

### ¿Qué es lo que realmente nos vamos a llevar de este taller?

- Introducción básica a JavaScript
- Comprender cómo se relaciona con HTML y CSS
- Aprender a jugar con JavaScript en nuestros exploradores
- Y algunas estrategias para:
- Elegir una librería de JavaScript
- Comprender rápidamente ejemplos de código
- Comenzar a (romper) modificar código JavaScript

---

### ¿Por qué es tan importante JavaScript?

- Funciona en todos los exploradores web.
- Es uno de los lenguajes de programación más populares y activos del mundo.
- Junto con HTML y CSS, JavasScript es una de las tres principales tecnologías de Internet.
- Es lo que permite que las páginas html sean dinámicas

---

#### [Thimble](http://thimbleprojects.org): una herramienta para aprender HTML, CSS y JavaScript

- Es un editor de código en línea
- Creado para editar y publicar páginas web
- Y al mismo tiempo aprender HTML, CSS y JavaScript
- Desarrollado por [Mozilla](https://www.mozilla.org), los creadores de [Firefox](https://www.mozilla.org/en-US/firefox/)

---

### HTML, CSS y JavaScript

- Son los tres lenguajes que nos permiten crear páginas web
- HTML: define la estructura básica de una página web
- CSS: define la presentación de la página
- JavaScript: define el comportamiento de la página web
- Permite modificar de manera dinámica tanto la estructura como la presentación de la página

---

### Un ejemplo de HTML, CSS y JavaScript

[Ver](https://thimbleprojects.org/opensas/533207) | [Remix](https://thimble.mozilla.org/es/projects/533207/remix)

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>¿Qué es JavaScript?</title>
    <style>
      p {
        font-family: 'helvetica neue', helvetica, sans-serif;
        letter-spacing: 1px;
        text-transform: uppercase;
        text-align: center;
        border: 2px solid rgba(0,0,200,0.6);
        background: rgba(0,0,200,0.3);
        color: rgba(0,0,200,0.6);
        box-shadow: 1px 1px 2px rgba(0,0,200,0.4);
        border-radius: 10px;
        padding: 3px 10px;
        display: inline-block;
        cursor:pointer;
      }
    </style>
  </head>
  <body>
    <h1>¿Quién le teme a <i>JavaScript</i>?</h1>
    <p>
      Participante temeroso: <strong>desconocido</strong>
    </p>
    <script>
      var parrafo = document.querySelector('p');

      parrafo.addEventListener('click', actualizarNombre);

      function actualizarNombre() {
        var nombre = prompt('¿Cómo te llamás?');
        if (nombre === '' || nombre === null) nombre = 'desconocido';
        parrafo.innerHTML = 'Participante temeroso: <strong>' + nombre + '</strong>';
      }
    </script>
  </body>
</html>
```

---

### JavaScript 101: Tipos de Datos

En el mundo de las computadoras todo es un dato, que es en esencia una secuencia de unos y ceros.

En JavaScript, cada dato es un "valor", el cual tiene un determinado "tipo" que especifica qué podemos hacer con él.

Hay dos tipos de datos: primitivos (o simples) y objetos (o complejos)

---

### JavaScript 101: Datos Simple

- _strings_: Cadenas de texto

- _numbers_: números

- _booleans_: valores lógicos,  verdadero o falso (true / false)

- _null_: valores vacíos

- _undefined_: valores que no han sido definidos aún

---

### Tipo de Datos: Jugando con la consola

- En chrome o Firefox presionamos F12 o botón derecho _Inspect_

- Seleccionamos la opción "Consola"

Note:

- la consola del depurador brinda autocomplete

---

#### Jugando con la consola: Strings

```javascript
>> 'cadena de texto'
<- "cadena de texto"

>> 'cadena de texto'.length
<- 15

>> typeof('cadena de texto')
<- "string"

>> 'cadena de texto' + 'otra cadena'
<- "cadena de textootra cadena"

>> 'abeja' > 'abaco'
<- true
```

---

#### Jugando con la consola: Strings

```javascript
>> 'cadena de texto'.substring(5,11)
<- "a de t"

>> 'cadena de texto'.split(' ')
<- Array(3) [ "cadena", "de", "texto" ]

>> 'cadena de texto'.startsWith('cadena')
<- true

>> 'cadena de texto'.startsWith('texto')
<- false

>> 'cadena de texto'.endsWith('texto')
<- true

>> 'cadena de texto'.toUpperCase()
<- "CADENA DE TEXTO"
```

---

#### Jugando con la consola: Numbers

```javascript
>> 22
<- 22

>> typeof(22)
<- "number"

>> (22).toString
<- ƒ toString() { [native code] }

>> (22).toString()
<- "22"

>> 22 < 22.00001
<- true
```

---

#### Jugando con la consola: Booleans

```javascript
>> // operadores logicos: NOT -> !, AND -> &&, OR -> ||

>> 1 === 2
<- false

>> !(1 === 2)
<- true

>> !false
<- true

>> 10 > 5 && 10 > 20
<- false

>> 10 > 5 || 10 > 20
<- true
```

---

### JavaScript 101: Variables

- Las variables son contenedores que almacenan un valor.

- Deben tener un nombre que debe ser único.

- Tenemos que declaralas con la sentencia 'let', 'var' o 'const'.

- Les asignamos valores con el operador "=".

- Podemos asignarles otros valores

---

#### Jugando con la consola: Variables

```javascript
>> let nombre
<- undefined

>> typeof(nombre)
<- "undefined"

>> nombre = 'Sebastian'
<- "Sebastian"

>> typeof(nombre)
<- "string"

>> nombre = 22
<- 22

>> typeof(nombre)
<- "number"
```

Note:

Las variables funcionan en remplazo del valor que contiene

---

#### JavaScript 101: Datos Complejos (Objetos)

- Arrays: tienen un conjunto de elementos a los cuales se accede mediante un índice númerico.

- Objetos: están compuestos por un conjunto de propiedades, cada una con un nombre y valor. Se accede al valor de cada propiedad mediante el nombre.

- Funciones: son programas que pueden ser ejecutados. Pueden recibir valores como parámetros y retornar un valor

---

#### Jugando con la consola: Arrays

```javascript
>> let miArray = [1, 'hola', true, null, 20]
<- undefined

>> miArray[0]
<- 1

>> miArray[1]
<- "hola"

>> miArray[2]
<- true

>> miArray[3]
<- null

>> miArray[4]
<- 20

>> miArray[5]
<- undefined

>> miArray.length
<- 5
```

---

#### Jugando con la consola: Objetos

```javascript
>> let evento = {
    nombre: 'MozFest',
    anio: 2018,
    espectacular: true,
    fecha: '26-10-2018',
    disertantes: [ 'Amba Kak', 'Zara Rahman', 'Malavika Jayaram', 'Renee DiResta', 'Tim Berners-Lee', 'et al...']
}
<- undefined

>> evento.nombre
<- "MozFest"

>> evento.web       // propiedad no definida
<- undefined

>> evento.web = 'https://mozillafestival.org/'   // agrego una nueva propiedad
<- "https://mozillafestival.org/"

>> evento.web
<- "https://mozillafestival.org/"
```

---

#### Jugando con la consola: Funciones

```javascript
>> function sorprendente(text) {
    return '¡' + text + '!';
}
<- undefined

>> sorprendente('MozFest 2018')
<- "¡MozFest 2018!"

>> function concatena(string1, string2) {
    return string1 + string2;
}

>> concatena('hola', 'mundo')
<- "holamundo"

>>> function primeraEnMayuscula(texto) {
    return texto.substr(0,1).toUpperCase() + texto.substr(1);
}
<- undefined

>>> primeraEnMayuscula('hola')
<- "Hola"
```

---

#### y por supuesto, todo esto se puede combinar y anidar

```javascript
let exponente = {
    nombre: 'sebastian scarano',
    alias: 'opensas',
    workshop: {
        nombre: 'Dr JavaScript',
        asistentes: [
            { nombre: 'Felipe Juárez', experiencia: 'principiante' },
            { nombre: 'Mariela Gutierrez', experiencia: 'avanzado' }
        ]
    },
    saludame: function() {
      return 'hola desde el usuario opensas';
    }
}

>>> usuario.workshop.asistentes[1].nombre
<- "Mariela Gutierrez"

>>> usuario.saludame
<- ƒ () {
      return 'hola desde el usuario opensas';
    }

>>> usuario.saludame()
<- 'hola desde el usuario opensas'
```
---

#### JSON: JavaScript Object Notation

- Es un formato de intercambio de información

- Está basado en la definición de objetos de JavaScript

- Es utilizado por multiples APIs

- Ejemplo: [UI](https://www.openstreetmap.org/search?query=Ravensbourne%20university%20london#map=18/51.50169/0.00568&layers=N) de OpenStreetMap versus [API](https://nominatim.openstreetmap.org/search?format=json&q=Ravensbourne%20University%20London) de Nominatim

---

### JavaScript 101: Bucles

```javascript
>> let palabras = ['bienvenidos', 'a', 'la', 'fiesta', 'de' 'Mozilla'];
<- undefined

>> let frase = ''
<- undefined

>> for (let i = 0; i < palabras.length; i++) {
  console.log(i, palabras[i]);
    frase = frase + palabras[i] + ' ';
}

<- 0 "bienvenidos"
<- 1 "a"
<- 2 "la"
<- 3 "fiesta"
<- 3 "de"
<- 4 "Mozilla"

>> frase
<- "bienvenidos a la fiesta de Mozilla "
```

---

### JavaScript 101: Bucles otra manera

```javascript
>> let palabras = ['bienvenidos', 'a', 'la', 'fiesta', 'de' 'Mozilla'];
<- undefined

>> let frase = ''
<- undefined

>> palabras.forEach( function(palabra) {
  frase = frase + palabra + ' ';
})
<- undefined

>> frase
<- "bienvenidos a la fiesta de Mozilla "
```

---

### JavaScript 101: Ejecución condicional

```javascript
>>> function esMayorDeEdad(edad) {
    if (edad >= 21) {
        return true;
    } else {
        return false;
    }
}
<- undefined

>>> esMayorDeEdad(20.9)
<- false

>>> esMayorDeEdad(21)
<- true

>>> function esMayorDeEdad(edad) { return (edad >= 21); }  // mas corto
<- undefined
```

---

### Finalmente: Leaflet js

- Es una de las librerías js más utilizadas para desplegar mapas

- Siguiendo esta [guía](https://leafletjs.com/examples/quick-start/) arrancamos con [este ejemplo](https://leafletjs.com/examples/quick-start/example.html)

- y ver si ahora entendemos algo!!!

Note:

- Mostrar en el debugger como va dibujando cada elemento: https://leafletjs.com/examples/quick-start/example.html

---

#### Ejemplo Leaflet Paso a paso 1/2

- 01.Partimos del ejemplo de la [Quick Start Guide](https://leafletjs.com/examples/quick-start/) de Leaflet ([thimble](https://thimbleprojects.org/opensas/561420) | [remix](https://thimble.mozilla.org/projects/561420/remix))

- 02.Agregamos algunos puntos ([thimble](https://thimbleprojects.org/opensas/561855) | [remix](https://thimble.mozilla.org/projects/561855/remix))

- 03.Creamos un array con los puntos ([thimble](https://thimbleprojects.org/opensas/561859) | [remix](https://thimble.mozilla.org/projects/561859/remix))

---

#### Ejemplo Leaflet Paso a paso 2/2

- 04.Creamos nuestra propia librería para manejar mapas ([thimble](https://thimbleprojects.org/opensas/561861) | [remix](https://thimble.mozilla.org/projects/561861/remix))

- 05.Cargamos dinámicamente los puntos a desplegar desde una [planilla de google docs](https://docs.google.com/spreadsheets/d/1x6T9KgohCCt5v519kTzD8xSLt5VW4khb5KPsWS6AYQs/edit#gid=0) ([thimble](https://thimbleprojects.org/opensas/561863) | [remix](https://thimble.mozilla.org/projects/561863/remix))

- 06.Otro ejemplo con [AirTable](https://airtable.com/tbl2QhZ4kU1Iouwt3/viwiEgE3SfGUFasW4) ([thimble](https://thimbleprojects.org/opensas/561866) | [remix](https://thimble.mozilla.org/projects/561866/remix))

---

#### Ejemplo Leaflet 01 - Primeros pasos

- Ver [thimble](https://thimbleprojects.org/opensas/561420) | [remix](https://thimble.mozilla.org/projects/561420/remix)

- Bajamos el [ejemplo inicial](https://leafletjs.com/examples/quick-start/) de [Leaflet](https://leafletjs.com/)

- Lo centramos en Londres (51.507567, -0.112267)

- Agregamos un callback para mostrar un popup con las coordenadas del punto seleccionado

---

#### Ejemplo Leaflet 02 - Agregamos puntos

- Ver [thimble](https://thimbleprojects.org/opensas/561855) | [remix](https://thimble.mozilla.org/projects/561855/remix)

- Agregamos 3 puntos al mapa

---

#### Ejemplo Leaflet 03 - Trabajando con arrays

- Ver [thimble](https://thimbleprojects.org/opensas/561859) | [remix](https://thimble.mozilla.org/projects/561859/remix)

- Creamos un array con los puntos

- Agregamos los puntos al mapa con un bucle que recorre el array

- Agregamos algunos puntos más

---

#### Ejemplo Leaflet 04 - Nuestra propia librería

- Ver [thimble](https://thimbleprojects.org/opensas/561861) | [remix](https://thimble.mozilla.org/projects/561861/remix)

- creamos la funcion _inicializarMapa_ con toda la funcionalidad general

- creamos el archivo _mapa.js_ con la funcion inicializarMapa

---

#### Ejemplo Leaflet 05 - [Google docs](https://docs.google.com/spreadsheets/)

- Crear una nueva [planilla de google docs](https://docs.google.com/spreadsheets/d/1x6T9KgohCCt5v519kTzD8xSLt5VW4khb5KPsWS6AYQs/edit#gid=0) con los puntos del mapa

- _Archivo_ -> _Publicar en la web_

- _Compartir_ -> _Configuración avanzada_ -> _Cualquier usuario con el vínculo_ - _puede ver_

- Copiar el _vínculo para compartir_

- Atención: ver [estas notas](https://gist.github.com/jsvine/3295633) antes de usar en producción

Note:

- Mostrar la [planilla](https://docs.google.com/spreadsheets/d/1x6T9KgohCCt5v519kTzD8xSLt5VW4khb5KPsWS6AYQs/edit#gid=0) de google docs

---

#### Ejemplo Leaflet 05 - Nuestro propio backend con [Google docs](https://docs.google.com/spreadsheets/)

- Ver [thimble](https://thimbleprojects.org/opensas/561863) | [remix](https://thimble.mozilla.org/projects/561863/remix)

- agregamos la librería [tabletop](https://github.com/jsoma/tabletop)

- creamos la funcion gdocsCargarPlanilla siguiendo [la documentación](https://github.com/jsoma/tabletop#2-setting-up-tabletop) de tabletop

- cargamos dinámicamente los puntos a partir de nuestra planilla en google docs

- [tabletop](https://github.com/jsoma/tabletop) accede a nuestra planilla mediante el [web service](https://spreadsheets.google.com/feeds/list/1x6T9KgohCCt5v519kTzD8xSLt5VW4khb5KPsWS6AYQs/od6/public/values?alt=json) de google

Note:

- Mostrar ejemplo de llamada json

---

#### Ejemplo Leaflet 06 - [Airtable](https://airtable.com)

- Es una base de datos personal en línea, una mezcla de Excel y Trello

- Creamos una [planilla](https://airtable.com/tbl2QhZ4kU1Iouwt3/viwiEgE3SfGUFasW4) con los puntos del mapa

- Generamos una _key_ para utilizar la [api](https://airtable.com/account)

- Consultamos la [documentación](https://airtable.com/api) de la api

- Usamos la [API](https://api.airtable.com/v0/appXYSHO92EL9VLc5/Table%201?api_key=keyx7FrPMoI1SDzTj) de Airtable desde JavaScript para traer los puntos

- Más info: [comparativa](https://stackshare.io/stackups/airtable-vs-sheetsee-js-vs-tabletop-js) entre [Airtable](https://airtable.com), [tabletop](https://github.com/jsoma/tabletop) y [Sheetsee.js](http://jlord.us/sheetsee.js)
---

#### Ejemplo Leaflet 06 - Nuestro propio backend con [Airtable](https://airtable.com)

- Ver [thimble](https://thimbleprojects.org/opensas/561866) | [remix](https://thimble.mozilla.org/projects/561866/remix)

- creamos una [planilla en airtable](https://airtable.com/tbl2QhZ4kU1Iouwt3/viwiEgE3SfGUFasW4) con los puntos del mapa

- creamos una librería para leer la información de airtable desde JavaScript

- cargamos dinámicamente los puntos a partir de nuestra planilla en airtable

---

#### Otro ejemplo: [C3.js](https://c3js.org/)

- Librería de gráficos basada en D3.js

- Tomamos el [ejemplo inicial](https://c3js.org/gettingstarted.html)

- Ver [thimble](https://thimbleprojects.org/opensas/534012) | [remix](https://thimble.mozilla.org/projects/534012/remix)

---

#### Ejemplo C3 paso 2 - exploramos opciones

- Ver [thimble](https://thimbleprojects.org/opensas/533110) | [remix](https://thimble.mozilla.org/projects/533110/remix)

- Exploramos todos [los ejemplos](https://c3js.org/examples.html) y tomamos las opciones que nos pueden ser útiles

---

#### Ejemplo C3 paso 3 - Nuestra propia librería

- Ver [thimble](https://thimbleprojects.org/opensas/534027) | [remix](https://thimble.mozilla.org/projects/534027/remix)

- Cargamos la configuración de los datos en un array

- Creamos una librería para crear el gráfico a partir del array

---

#### Ejemplo C3 paso 4 - Backend con [Google docs](https://docs.google.com/spreadsheets/)

- Ver [thimble](https://thimbleprojects.org/opensas/563309) | [remix](https://thimble.mozilla.org/projects/563309/remix)

- creamos una planilla en [Google docs](https://docs.google.com/spreadsheets/d/11oMuXUBENXrdnWzncfvR1yZTXjjf92FmJWfn3c8QRgg/edit#gid=0) y la compartimos

- agregamos la librería [tabletop](https://github.com/jsoma/tabletop)

- reutilizamos la funcion gdocsCargarPlanilla siguiendo [la documentación](https://github.com/jsoma/tabletop#2-setting-up-tabletop) de tabletop

- cargamos dinámicamente los valores a partir de nuestra planilla en google docs

---

#### Ejemplo C3 paso 5 - Backend con [Airtable](https://airtable.com)

- Ver [thimble](https://thimbleprojects.org/opensas/534086/) | [remix](https://thimble.mozilla.org/projects/534086/remix)

- creamos una planilla en [AirTable](https://airtable.com/tblMHxcsgnNMQy5LY/viwkFA2KuFQW9JjEE)

- reutilizamos la libreria _airtablePlanilla_ para leer la información de AirTable desde JavaScript

- generamos nuestra [apikey](https://airtable.com/account)

- consultamos la [documentacion](https://airtable.com/api) para ver conectarnos a nuestra planilla

- cargamos dinámicamente la información del gráfico a partir de nuestra planilla en airtable

---


#### Un ejemplo completo con JavaScript, Leaflet y la API de [Carto](https://carto.com/) (ex CartoDB)

- Mapa cultura: mapa interactivo con la información de entidades culturales de todo el país

- Presentado en la Media Party de Buenos Aires, mostrando el uso de [ckan](https://ckan.org), [OpenRefine](http://openrefine.org), [Carto](https://carto.com/), Sql, [GitHub](https://github.com/), JavaScript y [LeafLet](https://leafletjs.com).

---

#### Un ejemplo completo con JavaScript, Leaflet y la API de [Carto](https://carto.com/) (ex CartoDB)

- El proyecto completo en [Thimble](https://thimbleprojects.org/opensas/534091) | [remix](https://thimble.mozilla.org/projects/534091/remix)

- La aplicación corriendo en [www.nardoz.com/mapa-cultura](http://www.nardoz.com/mapa-cultura)

- Aquí está [la presentación](http://www.nardoz.com/mapa-cultura/slides/mediaparty_rendered.svg) y el [repositorio de GitHub](https://github.com/Nardoz/mapa-cultura)

Note:

mostar un ejemplo del consumo de la api:

- Ejemplo de consumo de la API de CartoDB: <a href="https://devel.cartodb.com/api/v2/sql?q=select%20tipo%2C%20subtipo%2C%20nombre%2C%20direccion%2C%20telefono%2C%20email%2C%20web%2C%20lat%2C%20lon%20from%20cultura%20where%20(ST_Within(the_geom%2C%20ST_Envelope(ST_GeomFromText(%27LINESTRING(%20-58.58865516919517%20-34.46638983527999%2C%20-58.26502067308998%20-34.78320404698995%20)%27%2C%204326))))%20and%20(lower(nombre)%20like%20%27%25corrientes%25%27%20or%20lower(direccion)%20like%20%27%25corrientes%25%27)%20and%20((lower(subtipo)%20in%20(%27salas%20de%20cine%27%2C%20%27espacios%20incaa%27)))">cines de la calle corrientes</a>

---

#### Otro ejemplo: una visualización de protesta

- Visualización realizada para denunciar la tercerización y vaciamiento de la Dirección de Sistemas del Ministerio de Trabajo de Argentina

- Visualización en línea: [construyendotrabajo.org](http://construyendotrabajo.org/)

- Repositorio en [github](https://github.com/MTEySS/mteyss.github.io)

- El [archivo json](http://construyendotrabajo.js.org/data.json) con la información de la visualización

---?image=assets/images/gitpitch-audience.jpg

### Muchas gracias a todos!

## ¿Preguntas?

<br>

@fa[twitter gp-contact]&nbsp;[@opensas](https://twitter.com/opensas)

@fa[github gp-contact]&nbsp;[opensas](https://github.com/opensas)

@fa[gitlab gp-contact]&nbsp;[opensas](https://gitlab.com/opensas/js_para_periodistas)

---

## Agradecimientos

<br>
@fa[github gp-contact]&nbsp;[ryanwarsaw](https://github.com/ryanwarsaw) - Ryan Warsaw

@fa[twitter gp-contact]&nbsp;[@gvilarino](https://twitter.com/gvilarino) - Guido Vilariño

@fa[twitter gp-contact]&nbsp;[@deimidis](https://twitter.com/deimidis) - Guillermo Movia

@fa[twitter gp-contact]&nbsp;[@mozilla](https://twitter.com/mozilla) - Mozilla