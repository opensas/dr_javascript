# Dr JavaScript

## O cómo aprendí a dejar de preocuparme y AMAR JavaScript

Recursos para el [Workshop interactivo de introducción a JavaScript](https://guidebook.com/guide/147793/event/21682683/) para periodistas para la [Mozfest 2018 de Mozilla](https://mozillafestival.org/).

## Descripción del Workshop

Ver la [presentación](https://gitpitch.com/opensas/dr_javascript?grs=gitlab#/?grs=gitlab#/) del workshop

No hace falta que tengas un título de _Ingeniero en Software_ para que puedas comenzar a jugar con JavaScript, pero a menudo esto es mucho más fácil decirlo que hacerlo.

En este workshop vas a aprender los rudimentos básicos de JavaScript como para poder utilizar y adaptar librerías populares de JS (Como C3.js, Leaftlet y otras) sin morir en el intento.

Para ello te presentaremos una serie de ejercicios interactivos y un tutorial "paso-a-paso", también aprenderás algunas estrategias para rápidamente poder entender qué hace una porción de código y empezar a adaptarlo a tus necesidades.

En este repositorio vas a encontrar los ejemplos de código utilizados, así como varios recursos que te resultarán de suma utilidad.

## Corriendo los ejemplos

Clonar el proyecto con

```bash
git clone https://gitlab.com/opensas/dr_javascript.git
```

O descargar el archivo zip desde [aquí](https://gitlab.com/opensas/dr_javascript/-/archive/master/dr_javascript-master.zip) y descomprimirlo

Luego

```
cd dr_javascript
```

Iniciá un [servidor local](local_server.md) y visitá el sitio. [Acá](local_server.md) tenés instrucciones para instalar y configurar varios servidores locales.

Allí vas a encontrar links para ejecutar los siguientes ejemplos.

Todos los ejemplos se encuentran en la carpeta [src](src/)

E iniciar un servidor http local:

## Contenido del Workshop

1. [Qué es JavaScript](https://gitpitch.com/opensas/dr_javascript?grs=gitlab#/3)

1. [Tipos de datos simples](https://gitpitch.com/opensas/dr_javascript?grs=gitlab#/8)

1. [Variables](https://gitpitch.com/opensas/dr_javascript?grs=gitlab#/14)

1. [Tipos de datos complejos: Arrays, Objectos y Funciones](https://gitpitch.com/opensas/dr_javascript?grs=gitlab#/16)

1. [JSON: JavaScript Object Notation](https://gitpitch.com/opensas/dr_javascript?grs=gitlab#/21)

1. [Bucles y ejecución condicional](https://gitpitch.com/opensas/dr_javascript?grs=gitlab#/22)

1. [Ejemplo con Leaflet.js](https://gitpitch.com/opensas/dr_javascript?grs=gitlab#/25)

1. [Ejemplo con C3.js](https://gitpitch.com/opensas/dr_javascript?grs=gitlab#/36)

1. [Mapa de Cultura: Un ejemplo completo con JavaScript, Leaflet y la API de Carto](https://gitpitch.com/opensas/dr_javascript?grs=gitlab#/42)

1. [Agradecimientos](https://gitpitch.com/opensas/dr_javascript?grs=gitlab#/45)

## Recursos

Repositorio en [GitLab](https://gitlab.com/): [https://gitlab.com/opensas/dr_javascript](https://gitlab.com/opensas/dr_javascript)

Diapositivas: [link](https://gitpitch.com/opensas/dr_javascript?grs=gitlab#/)

## Ejercicios de ejemplo

A continuación los ejemplos corriendo en [Thimble](https://thimble.mozilla.org). Sólo tienen que apretar el botón _Mezclar_/_Remix_ para crear una copia y comenzar a experimentar con el código.

### Qué es JavaScript

01. [Qué es JavaScript](src/02_que_es_js) | Ver en [thimble](https://thimbleprojects.org/opensas/533207) | [remix](https://thimble.himbleprojects.org/projects/533207/remix)

### Ejemplo con LeafLet.js

01. [Leaflet - ejemplo básico](src/leaflet_01) | Ver en [thimble](https://thimbleprojects.org/opensas/561420) | [remix](https://thimble.mozilla.org/projects/561420/remix)

02. [Leaflet - agregamos puntos](src/leaflet_02) | Ver en [thimble](https://thimbleprojects.org/opensas/561855) | [remix](https://thimble.mozilla.org/projects/561855/remix)

03. [Leaflet - trabajando con arrays](src/leaflet_03) | Ver en [thimble](https://thimbleprojects.org/opensas/561859) | [remix](https://thimble.mozilla.org/projects/561859/remix)

04. [Leaflet - creamos nuestra librería](src/leaflet_04) | Ver en [thimble](https://thimbleprojects.org/opensas/561861) | [remix](https://thimble.mozilla.org/projects/561861/remix)

05. [Leaflet - backend con google docs](src/leaflet_05) | Ver en [thimble](https://thimbleprojects.org/opensas/561863) | [remix](https://thimble.mozilla.org/projects/561863/remix)

06. [Leaflet - backend con AirTable](src/leaflet_06) | Ver en [thimble](https://thimbleprojects.org/opensas/561866) | [remix](https://thimble.mozilla.org/projects/561866/remix)

### Ejemplo con C3.js

01. [C3 - ejemplo básico](src/c3_01) | Ver en [thimble](https://thimbleprojects.org/opensas/534012) | [remix](https://thimble.mozilla.org/projects/534012/remix)

02. [C3 - investigamos opciones de configuración](src/c3_02) | Ver en [thimble](https://thimbleprojects.org/opensas/533110) | [remix](https://thimble.mozilla.org/projects/533110/remix)

03. [C3 - creamos nuestra librería](src/c3_03) | Ver en [thimble](https://thimbleprojects.org/opensas/534027) | [remix](https://thimble.mozilla.org/projects/534027/remix)

04. [C3 - backend con google docs](src/c3_04) | Ver en [thimble](https://thimbleprojects.org/opensas/563309) | [remix](https://thimble.mozilla.org/projects/563309/remix)

05. [C3 - backend con AirTable](src/c3_05) | Ver en [thimble](https://thimbleprojects.org/opensas/534086/) | [remix](https://thimble.mozilla.org/projects/534086/remix)

### Un ejemplo completo con JavaScript, Leaflet y la API de Carto

01. [Mapa de Cultura](src/mapa_cultura) | Ver en [thimble](https://thimbleprojects.org/opensas/534091) | [remix](https://thimble.mozilla.org/projects/534091/remix)

La aplicación corriendo en [www.nardoz.com/mapa-cultura](http://www.nardoz.com/mapa-cultura)

Diapositivas de la [presentación](http://www.nardoz.com/mapa-cultura/slides/mediaparty_rendered.svg)

[Repositorio de GitHub](https://github.com/Nardoz/mapa-cultura)