

### generate static site

doc: https://github.com/mixu/markdown-styles/blob/master/readme.md

generate-md --layout mixu-gray --input ./README.md --output ./output
cp output/README.html index.html

generate-md --layout mixu-gray --input ./local_server.md --output ./output
cp output/local_server.html .

http-serve

chromium output/README.html

--

layouts: with table of contents
- mixu-book
- mixu-bootstrap-2col
- mixu-gray
- mixu-radar

rest of the layouts
- bootstrap3
- github
- jasonm23-dark
- jasonm23-foghorn
- jasonm23-markdown
- jasonm23-swiss
- markedapp-byword
- mixu-bootstrap
- mixu-page
- roryg-ghostwriter
- thomasf-solarizedcssdark
- thomasf-solarizedcsslight
- witex
